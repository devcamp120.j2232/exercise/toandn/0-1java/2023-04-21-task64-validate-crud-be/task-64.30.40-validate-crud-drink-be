package com.devcamp.pizza365.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CDrink;
import com.devcamp.pizza365.repository.IDrinkRepository;

@Service
public class DrinkService {
  @Autowired
  IDrinkRepository pIDrinkRepository;

  public ArrayList<CDrink> getAllCDrinks() {
    ArrayList<CDrink> listDrink = new ArrayList<>();
    pIDrinkRepository.findAll().forEach(listDrink::add);
    return listDrink;
  }
}
